console.log("Hellow World?");

// Conditional Statement
	// Allows us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another seperate instruction if otherwise. 

// [ SECTION ] If, Else If, and Else Statement
	
	let numA = 0;

	// IF Statements

		if(numA <= 0){
			console.log("Hellow");
		};

		// if numA is less than 0, run console.log("Hellow");

		/*
		Syntax:

			if(condition){
				statement;
			};

		*/

		console.log(numA <= 0); // results will be true.

		// The result of expression added in the if's condition must result to true, elese the statement inside the if() will not run.

		// Let's update the variable and run an if statement with the same condition:

		numA = -1;

		if(numA > 0){
			console.log('Hellow again from numA is -1');
		};

		// This will not run because the expression now results to false:
		console.log(numA > 0);

		let city = "New York"

		if(city == "New York"){
			console.log('Welcome to New York City!');
		};

		console.log(city == "New York");

	
	// Else If Clause

		// Executes a statement if previous conditions are false and if the specified condition is true

		let numH = 1;

		numA = 1;

		if(numA < 0){ // the condition is now false, if false code block will not run
			console.log("Hellow");
		} else if (numH > 0){ // the condition now here is true, thus this will print console.log("Wurld?")
			console.log("Wurld?");
		};

		// We were abnle to run the else if() statement after we evaluated that the if condition was failed.

		// If the if() condition was passed and run, we wil no longer evaluate to else if() and end the process there. 

		let numB = 5;

		if(numB < 0){
			console.log("This will not run!");
		} else if (numB < 5){
			console.log("The value is less than 5.");
		} else if (numB < 10){
			console.log("The value is less than 10.");
		};

		// Let's now update the city variable and look at another example:

		city = "Tokyo";

		if(city === "New York"){
			console.log("Welcome to New York City!");
		} else if(city === 'Tokyo'){
			console.log("Welcome to Tokyo, Japan!");
		};

	// Else Statement

		// Executes a statement if all existing condition are false

		console.log(numA);

		if(numA < 0){
			console.log("Hello!");
		} else if(numH === 0){
			console.log('World');
		} else {
			console.log("Again!");
		};

		// Since both the preceding if and else if conditions failed, the else statement was run instead.

		numB = 21;

		if(numB < 0){
			console.log("This will not run!");
		} else if (numB < 5){
			console.log("The value is less than 5.");
		} else if (numB < 10){
			console.log("The value is less than 10.");
		} else if (numB < 15){
			console.log("The value is less than 15.");
		} else if (numB < 20){
			console.log("The value is less than 20.");
		} else {
			console.log("The value is greater than 20!")
		};

		
		// else {
		// 	console.log("Do you this will run?");
		// };

		// 

		// In fact, it results to an error.

		// else if(numH === 0){
		// 	console.log("World");
		// } else {
		// 	console.log('How about this one?');
		// };

		// Same goes for else if, there should be a preceding if() first.

	// If, Else If, and Else Statements with Functions

		// Most of the times we would like to use if, else if, and else statement with functions to control the flow of our application.

		let message = 'No Message!';
		console.log(message);

		function determineTyphoonIntensity(windSpeed){

			if (windSpeed < 30) {
		        return 'Not a typhoon yet.';
		    } else if (windSpeed <= 61) {
		        return 'Tropical depression detected.';
		    }
		    else if (windSpeed >= 62 && windSpeed <= 88) { 
		        return 'Tropical storm detected.';
		    }
		    else if (windSpeed >= 89 && windSpeed <= 117) { 
		        return 'Severe tropical storm detected.';
		    }
		    else {
		        return 'Typhoon detected.';
		    }
		};

		message = determineTyphoonIntensity(25);
		console.log(message);

	// Truthy and Falsy

		// In JS a truthy value is a value that is considered true when encountered in a Boolean context

		/*
		Falsy Values/Exception for truthy:
			1. False
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. Nan
		*/

		if(true){
			console.log("Truthy!");
		};
		if(1){
			console.log("Truthy!");
		};
		if([]){
			console.log("Truthy!");
		};

		// False Example
		if(false){
			console.log("Falsy!");
		};

		if(0){
			console.log("Falsy!");
		};

		if(undefined){
			console.log("Falsy!");
		};

		//Try-Catch-Finally Statement

			// try-catch statements are commonly used for error handling

			function showIntensityAlert(windSpeed){
				try{
					alerat(determineTyphoonIntensity(windSpeed));
				} catch(error){
					console.log(typeof error);
					console.log(error);

					console.warn(error.message);

				} finally {
					alert('Intensity updates will show new alert!');
					console.log("this");
				}
			}

			showIntensityAlert(56);