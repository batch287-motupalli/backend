let num = 2;

let getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

let address = [258, "Washington Ave NW", "California", "90011"];

const [doorNo, city, country, zipCode] = address;

console.log(`I live at ${doorNo} ${city}, ${country} ${zipCode}`);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weightInKgs : "1075",
	height: {
		feet: 20,
		inches: 3
	}
};

const {name, type, weightInKgs, height} = animal;

console.log(`${name} was a ${type}. He weighed at ${weightInKgs} kgs with a measurement of ${height["feet"]} ft ${height["inches"]} in.`);

arrayOfNumbers = [1,2,3,4,5];

arrayOfNumbers.forEach((x) => console.log(x));

let reduceNumbers = arrayOfNumbers.reduce((x,y) =>  x+y);

console.log(reduceNumbers);

class Dog{

	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const frankie= new Dog("Frankie", 5, "Miniature Dachshund");

console.log(frankie);