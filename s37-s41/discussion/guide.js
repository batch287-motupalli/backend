Session s37:
	touch index.js .gitignore guide.js
	npm init --y
	npm install express mongoose cors
	create our Course Model
	create our User Model

Session s38:
	create a route for checkEmail
	create a route for registerEmail
	npm install bcrypt
		use bcrypt in user s password
	npm install jsonwebtoken
	create a route for user authentication
	create a route for user details

Session s39:
	refactor the user details, added authorization
	create a route for add course, added authorization and only admin can add a course

Session s40:
	create a route retrieving all the courses
	create a route retrieving all active courses
	create a route retrieving specfic course
	create a route updating a course
	create a route for archiving a course