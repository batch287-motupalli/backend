const Course = require("../models/Course");

module.exports.addCourse = (data) => {

	console.log(data);

	if(data.isAdmin){

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation failed
			if(error){

				return false;

			// Course creation successful
			} else {

				return true;
			}
		})


	}

	// Since Promise.resolve() returns a resolved promise, the variable message will already be in a resolved status.
	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
	
};

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {
		return result;
	});
};

module.exports.activeCourses = () => {

	return Course.find({ isActive: true }).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

module.exports.archiveCourse = (data) => {

	if(data.isAdmin){

		let archivedCourse = {
			isActive: false
		};

		return Course.findByIdAndUpdate(data.courseId, archivedCourse).then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
}