const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({ email: reqBody.email }).then(result => {
		
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			
			return true
		
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			
			return false
		
		}
	})
}

module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches
	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		// Email doesn't exist
		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// Correct password
			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			// Password incorrect	
			} else {

				return false
				
			};

		};
	});
};

module.exports.getProfile = (data) => {

	console.log(data); // Will return the key/value of pair of userId: data.Id

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	})
};

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({ courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({ userId: data.userId});

		return course.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	}
}