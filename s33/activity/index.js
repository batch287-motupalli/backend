fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => console.log(json.map(item => item['title'])));

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => {
				console.log(json);
				console.log(`The item "${json.title}" on the list has a status of ${json.completed}`);
			}
	);

fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			completed: false,
			title: "Created To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1,
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			status: "Complete",
			dateCompleted : "07/09/21"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'DELETE'
	});