// Defines WHEN particular controllers will be used
// Contains all the endpoints for our application

// We separate the routes such that "app.js" only contains information on the server

// We will need to use express Router() function to achieve this
const express = require("express");

// Creates a Router instance that function as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// [ SECTION ] Routes

// Routes to get all the tasks
router.get("/", (req, res) => {

	// Invoke the "getAllTasks()" function from the "controller.js" file and return the result back to the client/Postman

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Delete Task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL

router.delete("/:id", (req, res) => {

	// URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	// If information will be coming from the URL, the data can be accessed from the request "params" property
	taskController.deleteTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


// Updating a task 

/*
Business Logic
1. Get the task with the id using the Mongoose method "findById"
2. Replace the task's name returned from the database with the "name" property from the request body
3. Save the task
*/


router.put("/:id", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});

module.exports = router;