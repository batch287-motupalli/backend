const express = require("express");

const router = express.Router();

const taskController = require("../controllers/taskController");

router.get("/:id", (req, res) => {

	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});

module.exports = router;