const Task = require("../models/task");

module.exports.getTask = (taskId) => {

	return Task.findById(taskId).then((result,err) => {

		if(err){

			console.log(err);
			return false;

		} else {
			
			return result;
		}
	});
};

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err);
			return false;
		}

		result.status = "complete";

		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){

				console.log(saveErr); 
				return false;

			} else {

				return result;
			};
		});
	});
};
