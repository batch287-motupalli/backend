// CRUD Operations
    // CRUD Operations are the heart of any backend application
    // Mastering the CRUD Operations is essential for any developer

// [ SECTION ] Inserting Documents (CREATE)

// Insert One Documents
    // Creating MongoDB Syntax in a text editor makes it easy for us to modify and create our code as opposed to typing is directly
    // Syntax:
        // db.collectionName.insertOne({object})

db.users.insertOne({
    "firstName": "John",
    "lastName": "Smith"
});

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "123456789",
        email: "janedoe@gamil.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
});

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "123456789",
            email: "theoryofeverything@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 76,
        contact: {
            phone: "123456789",
            email: "firstmaninthemoon@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);

// [ SECTION ] Finding Documents (READ)

    // Find
        // If multiple documents match the criteria for finding a document onlythe First document  that matches the search term will be returned

    // Syntax:
        db.collectionName.find();
        db.collectionName.find({field: value});

    db.users.find();
    db.users.find({firstName: "Stephen"});

// [ SECTION ] Updating Documents (Update)

    // Updating a Single Document

    db.users.insertOne({
        firstName: "Test",
        lastName: "Test",
        age: 0,
        contact: {
            phone: "0000000000",
            email: "test@mail.com"
        },
        course: [],
        department: "none"
    });

    // To update firstName: "Test"
    // Syntax:
        db.collectionName.updateOne({criteria},{$set {field: value}});

    db.users.updateOne(
        {firstName: "Test"},
        {
            $set: {
                firstName: "Bill",
                lastName: "Gates",
                age: 65,
                contact: {
                    phone: "09876543321",
                    email: "billgates@mail.com"
                },
                course: ["PHP", "C++", "HTML"],
                department: "Operations",
                status: "active"
            }
        }
    );

    // Update Many

    // Syntax:
        db.collectionName.updateMany( {criteria}, { $set: { field: value}});

    db.users.updateMany(
        {department: "none"},

        { 
            $set: {
                department: "HR"
            }
        }
    )

// [ SECTION ] Deleting Documents (DELETE)

    // Delete One

        // Syntax:
            db.collectionName.deleteOne({ Criteria });

    db.users.insertOne({
        firstName: "Test"
    });

    db.users.deleteOne({
        firstName: "Test"
    });

    // Delete Many
        // Syntax:
            db.collectionName.deleteMany({ criteria });