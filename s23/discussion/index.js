console.log("Hellow WurlD?");

// [ SECTION ] Objects
	// An object is a data type that is used to represent real world objects
	// Information stored in objects are repsented in a "key:value" pair

	// Creating object using object initializers/literal notation

	// Syntax:
		// let objectName = {
			// keyA: valueA,
			// keyB: valueB
		//};

	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
	};

	console.log('Creating object using object initializers/literal notation:');
	console.log(cellphone);
	console.log(typeof cellphone);

	// Creating object using a constructor function

		// Creates a reusable function to create several object that have the same data structure

		// Syntax:
			// function objectName(keyA, keyB){
				// this.keyA = ketA,
				// this.keyB = keyB
			//}

		// This is an object
		// The "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function's parameters

	function Shirt (color, day){
		this.colorSelected = color;
		this.daySelected = day;
	};

	// The "new" operator creates an instance of an object

	let shirt = new Shirt ('Red', "Monday");
	console.log("Result of creating object using a constructors: ");
	console.log(shirt);

	let dress = new Shirt ('Blue', "Tuesday");
	console.log("Result of creating object using a constructors: ");
	console.log(dress);

	let socks = Shirt ('Yellow', "Thursday");
	console.log("Result of creating object using a constructors: ");
	console.log(socks);

		// Unable call that new operatore it will return undefined without the 'new' operator
		// The behavior for this is is like calling/invoking the Laptop function instead of creating a new object instance.

	// Create empty objects
	let computer = {};
	let myComputer = new Object();

	// [ SECTION ] Accessing Object Properties

	// Using the dot notation
	console.log('Result from dot notation: ' + shirt.colorSelected);

	// Using the square bracket notation
	console.log('Result from square bracket notation: ' + shirt['daySelected']);

	// Accessing Array Objects

		// Accessing array elements can be also done using square brackets

	let array = [ shirt, dress ];
	console.log(array);

	console.log(array[0]['daySelected']);
	// This tells us that array[0] is an object by using the dot notation
	console.log(array[0].daySelected);

	let car = {};

	// Initilizing/Adding object properties using dot notation

	car.name = 'Honda Civic';
	console.log("Result from Adding object properties using dot notation");
	console.log(car);

	// Initilizing/Adding object properties using bracket notation

	car['manufacture date'] = 2023;
	console.log(car['manufacture date']);
	console.log(car['manufacture Date']);
	console.log(car.manufacturedate);
	console.log('Result from Adding object properties using bracket notation');
	console.log(car);

	// Deleting object properties
	delete car['manufacture date'];
	console.log('Result from deleting properties');
	console.log(car);

	// Reassinging object properties
	car.name = 'Dodge Charger R/T';
	console.log('Result from reassinging properties');
	console.log(car);

// [ SECTION ] Object Methods
	// A method is a function which is a property of an object

	let person = {
		name: 'John',
		talk: function(){
			console.log('Hello my name is ' + this.name);
		}
	};

	console.log(person);
	console.log('Result from object method:');
	person.talk();

	// Methods are useful for creating reusable functions that perform tasks related to object

	let friend = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: "Austin",
			country: 'Texas'
		},
		email: ['joe@mail.com', 'joesmith@mail.xyz'],
		introduce: function(){
			console.log('Hellow my name is ' + this.firstName + " " + this.lastName);

		},
		introduceAddress: function(){
			console.log('I currently lived in ' + this.address.city + ", " + this.address.country + "!");

		},
		introduceContact: function(){
			console.log('My email address follows: ' + this.email[0] + ", " + this.email[1]);

		}
	};

	friend.introduce();
	friend.introduceAddress();
	friend.introduceContact();


	// Mini-Activity,
	// Generate a function introduceAddress, that basically inform Joe's address
	// Generate a function introduceContanctDetails, that basically inform Joe's contact details
	// Trigger both function
	// Time Check 11:45, let's check this around 11:52