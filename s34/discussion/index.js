// Use the require directive to load the express module
// It allow us to access to methods and functions that will allow us to easily create a server
const express = require("express");

// Creates an application using express 
// In layman's term, app is our server
const app = express();

// For our application to run, we need a port to listen to
const port = 3001;

// Use middleware to allow express to read JSON
app.use(express.json());

// Use middleware to allows express to able to read more data types from a response
app.use(express.urlencoded({extended:true}));

// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send('Hellow from the /hello endpoint!');
});

app.post("/display-name", (req, res) => {
	res.send(`Hellow there ${req.body.firstName} ${req.body.lastName}!`);

});

// Sign-up in our users

let users = []; // This is our mockdatabases

app.post("/signup", (request, response) =>{

	if( request.body.username == "" || request.body.password == ""){
		response.send("Unable to create account\nUsername and/or password should not be empty");
	}
	else{
		users.push(request.body);
		response.send("Created account successfully");
	}
});

app.put("/change-password", (req, res) => {

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `Users ${req.body.username}'s password has been updated`;

			console.log("Newly updated mock database:");
			console.log(users);

			break;

		} else {
			message = "User does not exist";
		}
	}

	res.send(message);
})

app.listen(port, () => console.log(`Server is currently running at port ${port}`));