const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get("/home", (request, response) => {

	response.send('Welcome to the home page');
});

let users = [
		{
			username: "yaswanth",
			password: "yaswanth1234"
		},
		{
			username: "rohith",
			password: "rohith5678"
		},
		{
			username: "ajay",
			password: "ajay9012"
		},
		{
			username: "rakshan",
			password: "rakshan3456"
		},
		{
			username: "venkat",
			password: "venkat7890"
		},
		{
			username: "srinikethan",
			password: "srinikethan1234"
		},	
	];

app.get("/users", (request, response) => {

	response.send(users);
});

app.delete("/delete-user", (request, response) => {

	let message = `User ${request.body.username} not found!!`

	for(let i=0; i < users.length; i++){

		if(request.body.username == users[i].username){

			users.splice(i,1);

			message = `User ${request.body.username} has been deleted`;

			break;
		}
	}

	response.send(message);
});

app.listen(port, () => console.log(`Server is currently running at port ${port}`));