const http = require('http');

const port = 4004;

const server = http.createServer((request, response) => {

	// Accessing the 'greeting' routes returns a message of "Welcome to the server! This is the " 
	if(request.url == '/greeting'){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the server! This is currently running at local host:4004')

	} else if (request.url == '/homepage'){

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the homepage! This is currently running at local host:4004')
	} else{

		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page Not Available!!')
	}
})

// Uses the "server" and "port" variables created above.
server.listen(port);

// When server is running, console will print the message
console.log(`Server is now accesible at localhost:${port}.`)