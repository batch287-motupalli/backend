let number = Number(prompt("Give me a Number:"));

console.log("The number you provided is " + number + ".");

for(number; number > 0; number--){

	if(number <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if(number % 5 === 0){
		console.log(number);
	}
};

let string = "supercalifragilisticexpialidocious";

let stringConsonants = "";

for(let i = 0; i < string.length; i++){

	if(
		string[i].toLowerCase() === 'a' ||
		string[i].toLowerCase() === 'e' ||
		string[i].toLowerCase() === 'i' ||
		string[i].toLowerCase() === 'o' ||
		string[i].toLowerCase() === 'u' 
	  ){
		continue;
	} else{
		stringConsonants += string[i];
	}
}

console.log(string);
console.log(stringConsonants);